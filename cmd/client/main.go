package main

import (
	"context"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"time"

	pb "flex_cache/pkg/grpcapi"
	"google.golang.org/grpc"
)

// TODO Example client
func main() {

	conn, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewKeyValueServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// SET
	rSet, err := c.Set(ctx, &pb.SetRequest{Key: "testKey", Value: "testValue"})
	if err != nil {
		log.Fatalf("could not set: %v", err)
	}
	log.Printf("Set: %v", rSet.GetSuccess())

	// GET
	r, err := c.Get(ctx, &pb.GetRequest{Key: "testKey"})
	if err != nil {
		log.Fatalf("could not get: %v", err)
	}
	log.Printf("Get: %s", r.GetValue())

	// DELETE
	rDel, err := c.Delete(ctx, &pb.DeleteRequest{Key: "testKey"})
	if err != nil {
		log.Fatalf("could not delete: %v", err)
	}
	log.Printf("Delete: %v", rDel.GetSuccess())

}
