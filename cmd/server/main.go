package main

import (
	"context"
	"flex_cache/internal/service"
	"flex_cache/internal/storage"
	"flex_cache/internal/storage/memory"
	"flex_cache/internal/storage/multilevel"
	"flex_cache/internal/storage/redis"

	"github.com/sirupsen/logrus"
	"net"
	"os"
	"os/signal"
	"syscall"

	"flex_cache/internal/config"
	"flex_cache/internal/handler"
	"flex_cache/pkg/grpcapi"
	"google.golang.org/grpc"
)

var log = logrus.New()

func main() {
	cfg, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("Failed to load configuration: %v", err)
	}

	grpcServer := grpc.NewServer()
	log.SetLevel(logrus.InfoLevel)

	if cfg.LogLevel == "debug" {
		log.SetLevel(logrus.DebugLevel)
		grpcServer = grpc.NewServer(
			grpc.UnaryInterceptor(UnaryLoggingInterceptor(log)),
			grpc.StreamInterceptor(StreamLoggingInterceptor(log)),
		)
	}

	lis, err := net.Listen("tcp", cfg.ServerAddress)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	var storageImpl storage.Storage

	switch cfg.StorageType {
	case "redis":
		storageImpl = redis.NewRedisStorage(cfg.RedisAddress)
	case "memory":
		storageImpl = memory.NewMemoryStorage()
	case "multi-level":
		redisStorage := redis.NewRedisStorage(cfg.RedisAddress)
		memoryStorage := redis.NewRedisStorage(cfg.RedisAddress)
		storageImpl = multilevel.NewMultiLevelStorage(memoryStorage, redisStorage)
	default:
		log.Fatalf("Unsupported storage type: %s", cfg.StorageType)
	}

	log.Infof("Starting server with %s caching", cfg.StorageType)

	serviceImpl := service.NewServiceImpl(storageImpl)
	grpcHandler := handler.NewGrpcHandler(serviceImpl)

	// Registration GRPC server
	grpcapi.RegisterKeyValueServiceServer(grpcServer, grpcHandler)

	go func() {
		log.Printf("Server listening at %v", lis.Addr())
		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("Failed to serve: %v", err)
		}
	}()

	// Graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	<-c
	grpcServer.GracefulStop()
	log.Println("Shutting down the server...")
}

// UnaryLoggingInterceptor logs all unary requests.
func UnaryLoggingInterceptor(logger *logrus.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		logger.Infof("Unary request - Method: %s; Request: %+v", info.FullMethod, req)
		resp, err = handler(ctx, req)
		if err != nil {
			logger.Errorf("Unary error - Method: %s; Error: %v", info.FullMethod, err)
		}
		return resp, err
	}
}

// StreamLoggingInterceptor logs all streaming requests.
func StreamLoggingInterceptor(logger *logrus.Logger) grpc.StreamServerInterceptor {
	return func(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		logger.Infof("Stream request - Method: %s", info.FullMethod)
		err := handler(srv, ss)
		if err != nil {
			logger.Errorf("Stream error - Method: %s; Error: %v", info.FullMethod, err)
		}
		return err
	}
}
