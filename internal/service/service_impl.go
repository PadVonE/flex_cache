package service

import (
	"context"
	"flex_cache/internal/storage"
)

// serviceImpl implements the Service interface.
type serviceImpl struct {
	storage storage.Storage
}

// NewServiceImpl creates a new instance of serviceImpl with the specified storage.
func NewServiceImpl(storage storage.Storage) Service {
	return &serviceImpl{storage: storage}
}

// Get retrieves the value by key from the storage.
func (s *serviceImpl) Get(ctx context.Context, key string) (string, error) {
	return s.storage.Get(key)
}

// Set sets the value for the key in the storage.
func (s *serviceImpl) Set(ctx context.Context, key, value string) error {
	return s.storage.Set(key, value)
}

// Delete removes the key from the storage.
func (s *serviceImpl) Delete(ctx context.Context, key string) error {
	return s.storage.Delete(key)
}
