package service

import "context"

// Service defines the interface for the application's business logic.
type Service interface {
	Get(ctx context.Context, key string) (string, error)
	Set(ctx context.Context, key, value string) error
	Delete(ctx context.Context, key string) error
}
