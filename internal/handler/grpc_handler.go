package handler

import (
	"context"
	"flex_cache/internal/service"
	"flex_cache/pkg/grpcapi"
)

// GrpcHandler implements grpcapi.KeyValueServiceServer, handling GRPC calls.
type GrpcHandler struct {
	service service.Service
	grpcapi.UnimplementedKeyValueServiceServer
}

// NewGrpcHandler creates a new instance of GrpcHandler with the provided business logic.
func NewGrpcHandler(service service.Service) *GrpcHandler {
	return &GrpcHandler{service: service}
}

// Get handles a GRPC call to retrieve a value by key.
func (h *GrpcHandler) Get(ctx context.Context, req *grpcapi.GetRequest) (*grpcapi.GetResponse, error) {
	value, err := h.service.Get(ctx, req.Key)
	if err != nil {
		return nil, err
	}
	return &grpcapi.GetResponse{Value: value}, nil
}

// Set handles a GRPC call to set a value by key.
func (h *GrpcHandler) Set(ctx context.Context, req *grpcapi.SetRequest) (*grpcapi.SetResponse, error) {
	err := h.service.Set(ctx, req.Key, req.Value)
	if err != nil {
		return nil, err
	}
	return &grpcapi.SetResponse{Success: true}, nil
}

// Delete handles a GRPC call to delete a value by key.
func (h *GrpcHandler) Delete(ctx context.Context, req *grpcapi.DeleteRequest) (*grpcapi.DeleteResponse, error) {
	err := h.service.Delete(ctx, req.Key)
	if err != nil {
		return nil, err
	}
	return &grpcapi.DeleteResponse{Success: true}, nil
}
