package config

import (
	"os"
)

// Config contains the application configuration, including server and storage settings.
type Config struct {
	ServerAddress string
	StorageType   string
	RedisAddress  string
	LogLevel      string
}

// LoadConfig loads the application configuration from environment variables.
func LoadConfig() (*Config, error) {
	return &Config{
		LogLevel:      getEnv("LOG_LEVEL", "debug"),
		ServerAddress: getEnv("SERVER_ADDRESS", ":50051"),
		StorageType:   getEnv("STORAGE_TYPE", "multi-level"),
		RedisAddress:  getEnv("REDIS_ADDRESS", "localhost:6379"),
	}, nil
}

// getEnv returns the value of an environment variable or a fallback value if the variable is not defined.
func getEnv(key, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultValue
}
