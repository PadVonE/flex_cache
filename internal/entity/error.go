package entity

import "errors"

// ErrKeyNotFound defines an error for situations where the key is not found in the storage.
var ErrKeyNotFound = errors.New("key not found")
