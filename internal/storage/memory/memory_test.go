package memory

import (
	"flex_cache/internal/entity"
	"testing"
)

func TestMemoryStorage_Set(t *testing.T) {
	storage := NewMemoryStorage()

	// Test setting a value
	err := storage.Set("key1", "value1")
	if err != nil {
		t.Errorf("Failed to set a value: %v", err)
	}

	// Verify the value is set correctly
	value, _ := storage.Get("key1")
	if value != "value1" {
		t.Errorf("Expected value 'value1', got '%s'", value)
	}
}

func TestMemoryStorage_Get(t *testing.T) {
	storage := NewMemoryStorage()
	if err := storage.Set("key2", "value2"); err != nil {
		t.Fatalf("Set() failed with error: %v", err)
	}

	tests := []struct {
		key      string
		expected string
		exists   bool
	}{
		{"key2", "value2", true},
		{"nonexistent", "", false},
	}

	for _, tt := range tests {
		value, err := storage.Get(tt.key)

		if tt.exists && err != nil {
			t.Errorf("Get() returned unexpected error for key %v: %v", tt.key, err)
		}
		if !tt.exists && err == nil {
			t.Errorf("Get() for nonexistent key %v did not return an error", tt.key)
		}
		if !tt.exists && err != entity.ErrKeyNotFound {
			t.Errorf("Get() for nonexistent key %v returned wrong error: got %v, want %v", tt.key, err, entity.ErrKeyNotFound)
		}

		if value != tt.expected {
			t.Errorf("Get() value mismatch for key %v: expected '%v', got '%v'", tt.key, tt.expected, value)
		}
	}
}
func TestMemoryStorage_Delete(t *testing.T) {
	storage := NewMemoryStorage()

	if err := storage.Set("key3", "value3"); err != nil {
		t.Fatalf("Set() failed with error: %v", err)
	}

	err := storage.Delete("key3")
	if err != nil {
		t.Errorf("Failed to delete a value: %v", err)
	}

	value, err := storage.Get("key3")
	if err != entity.ErrKeyNotFound {
		t.Errorf("Expected ErrKeyNotFound for deleted key, got '%s' with error: %v", value, err)
	}
	if value != "" {
		t.Errorf("Expected no value for deleted key, got '%s'", value)
	}
}
