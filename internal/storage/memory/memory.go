package memory

import (
	"flex_cache/internal/entity"
	"sync"
)

// MemoryStorage implements the Storage interface, providing an in-memory data store.
type MemoryStorage struct {
	data map[string]string
	mu   sync.RWMutex
}

// NewMemoryStorage creates a new instance of MemoryStorage.
func NewMemoryStorage() *MemoryStorage {
	return &MemoryStorage{
		data: make(map[string]string),
	}
}

// Get returns the value for a specified key.
func (m *MemoryStorage) Get(key string) (string, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()
	value, exists := m.data[key]
	if !exists {
		return "", entity.ErrKeyNotFound
	}
	return value, nil
}

// Set sets the value for a specified key.
func (m *MemoryStorage) Set(key, value string) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.data[key] = value
	return nil
}

// Delete removes the key and its value from the storage.
func (m *MemoryStorage) Delete(key string) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	delete(m.data, key)
	return nil
}
