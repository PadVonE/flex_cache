package multilevel_test

import (
	"flex_cache/internal/entity"
	"flex_cache/internal/storage"
	"flex_cache/internal/storage/multilevel"
	"testing"

	gomock "go.uber.org/mock/gomock"
)

func TestMultiLevelStorage_Get(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	memoryMock := storage.NewMockStorage(ctrl)
	redisMock := storage.NewMockStorage(ctrl)

	mlStorage := multilevel.NewMultiLevelStorage(memoryMock, redisMock)

	tests := []struct {
		name          string
		key           string
		value         string
		setupMocks    func(memory *storage.MockStorage, redis *storage.MockStorage)
		expectedValue string
		expectedError bool
		testType      string
	}{
		{
			name:  "get from memory",
			key:   "keyInMemory",
			value: "valueInMemory",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Get("keyInMemory").Return("valueInMemory", nil)
			},
			expectedValue: "valueInMemory",
			expectedError: false,
			testType:      "get",
		},
		{
			name:  "get from Redis and cache in memory",
			key:   "keyInRedis",
			value: "valueInRedis",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Get("keyInRedis").Return("", entity.ErrKeyNotFound)
				redis.EXPECT().Get("keyInRedis").Return("valueInRedis", nil)
				memory.EXPECT().Set("keyInRedis", "valueInRedis").Return(nil)
			},
			expectedValue: "valueInRedis",
			expectedError: false,
			testType:      "get",
		},
		{
			name: "key not found in both storages",
			key:  "unknownKey",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Get("unknownKey").Return("", entity.ErrKeyNotFound)
				redis.EXPECT().Get("unknownKey").Return("", entity.ErrKeyNotFound)
			},
			expectedValue: "",
			expectedError: true,
			testType:      "get",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setupMocks(memoryMock, redisMock)

			var err error
			var value string

			value, err = mlStorage.Get(tt.key)
			if value != tt.expectedValue || (err != nil) != tt.expectedError {
				t.Errorf("Unexpected result for %s. Got value: %s, Got error: %v", tt.name, value, err)
			}

		})
	}
}

func TestMultiLevelStorage_Set(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	memoryMock := storage.NewMockStorage(ctrl)
	redisMock := storage.NewMockStorage(ctrl)

	mlStorage := multilevel.NewMultiLevelStorage(memoryMock, redisMock)

	tests := []struct {
		name          string
		key           string
		value         string
		setupMocks    func(memory *storage.MockStorage, redis *storage.MockStorage)
		expectedValue string
		expectedError bool
		testType      string
	}{
		{
			name:  "set in both storages",
			key:   "newKey",
			value: "newValue",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Set("newKey", "newValue").Return(nil)
				redis.EXPECT().Set("newKey", "newValue").Return(nil)
			},
			expectedError: false,
			testType:      "set",
		},
		{
			name:  "set existing key in both storages",
			key:   "existingKey",
			value: "newValue",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Set("existingKey", "newValue").Return(nil)
				redis.EXPECT().Set("existingKey", "newValue").Return(nil)
			},
			expectedError: false,
			testType:      "set",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setupMocks(memoryMock, redisMock)

			err := mlStorage.Set(tt.key, tt.value)
			if (err != nil) != tt.expectedError {
				t.Errorf("Unexpected error for %s. Got error: %v", tt.name, err)
			}

		})
	}
}

func TestMultiLevelStorage_Delete(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	memoryMock := storage.NewMockStorage(ctrl)
	redisMock := storage.NewMockStorage(ctrl)

	mlStorage := multilevel.NewMultiLevelStorage(memoryMock, redisMock)

	tests := []struct {
		name          string
		key           string
		value         string
		setupMocks    func(memory *storage.MockStorage, redis *storage.MockStorage)
		expectedValue string
		expectedError bool
		testType      string
	}{
		{
			name: "delete in both storages",
			key:  "keyToDelete",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Delete("keyToDelete").Return(nil)
				redis.EXPECT().Delete("keyToDelete").Return(nil)
			},
			expectedError: false,
			testType:      "delete",
		},
		{
			name: "delete non-existing key from both storages",
			key:  "nonExistingKeyToDelete",
			setupMocks: func(memory *storage.MockStorage, redis *storage.MockStorage) {
				memory.EXPECT().Delete("nonExistingKeyToDelete").Return(nil)
				redis.EXPECT().Delete("nonExistingKeyToDelete").Return(nil)
			},
			expectedError: false,
			testType:      "delete",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.setupMocks(memoryMock, redisMock)

			err := mlStorage.Delete(tt.key)
			if (err != nil) != tt.expectedError {
				t.Errorf("Unexpected error for %s. Got error: %v", tt.name, err)
			}

		})
	}
}
