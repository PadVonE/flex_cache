package multilevel

import (
	"flex_cache/internal/storage"
)

// MultiLevelStorage provides a multi-level storage solution using both in-memory and Redis stores.
type MultiLevelStorage struct {
	memory storage.Storage
	redis  storage.Storage
}

// NewMultiLevelStorage creates a new instance of MultiLevelStorage.
func NewMultiLevelStorage(memory, redis storage.Storage) *MultiLevelStorage {
	return &MultiLevelStorage{
		memory: memory,
		redis:  redis,
	}
}

func (s *MultiLevelStorage) Get(key string) (string, error) {
	// Attempt to retrieve the value from the in-memory store
	value, err := s.memory.Get(key)
	if err == nil {
		return value, nil
	}

	// If the value is not found in in-memory, search in Redis
	value, err = s.redis.Get(key)
	if err == nil {
		// If the value is found in Redis, cache it in in-memory for future requests
		_ = s.memory.Set(key, value)
		return value, nil
	}

	return "", err
}

// Set sets the value in both stores: in-memory and Redis.
func (s *MultiLevelStorage) Set(key, value string) error {
	err := s.memory.Set(key, value)
	if err != nil {
		return err
	}
	return s.redis.Set(key, value)
}

// Delete removes the key from both stores: in-memory and Redis.
func (s *MultiLevelStorage) Delete(key string) error {
	err := s.memory.Delete(key)
	if err != nil {
		return err
	}
	return s.redis.Delete(key)
}
