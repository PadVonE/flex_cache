package redis

import (
	"testing"

	"github.com/alicebob/miniredis/v2"
)

func TestRedisStorage(t *testing.T) {
	// Start a Miniredis server for testing
	mr, err := miniredis.Run()
	if err != nil {
		t.Fatalf("Error starting miniredis: %v", err)
	}
	defer mr.Close()

	storage := NewRedisStorage(mr.Addr())

	tests := []struct {
		name      string
		key       string
		value     string
		wantError bool
		wantValue string
		testType  string
	}{
		{"set value", "key1", "value1", false, "", "set"},
		{"get existing value", "key1", "", false, "value1", "get"},
		{"get non-existing value", "nonexistent", "", false, "", "get"},
		{"delete existing key", "key1", "", false, "", "delete"},
		{"get deleted key", "key1", "", false, "", "get"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var err error
			var val string

			switch tt.testType {
			case "set":
				err = storage.Set(tt.key, tt.value)
			case "get":
				val, err = storage.Get(tt.key)
				if val != tt.wantValue {
					t.Errorf("Got value %s, want %s", val, tt.wantValue)
				}
			case "delete":
				err = storage.Delete(tt.key)
			}

			if (err != nil) != tt.wantError {
				t.Errorf("Error = %v, wantError %v", err, tt.wantError)
			}

			if tt.testType == "delete" {
				_, err := storage.Get(tt.key)
				if err != nil || val != "" {
					t.Errorf("Expected empty value for deleted key, got '%s' with error: %v", val, err)
				}
			}
		})
	}
}
