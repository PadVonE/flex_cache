package redis

import (
	"context"
	"github.com/go-redis/redis/v8"
)

// RedisStorage implements the Storage interface for data storage in Redis.
type RedisStorage struct {
	client *redis.Client
}

// NewRedisStorage creates a new instance of RedisStorage.
func NewRedisStorage(addr string) *RedisStorage {
	client := redis.NewClient(&redis.Options{
		Addr: addr,
	})

	return &RedisStorage{
		client: client,
	}
}

// Get retrieves the value for a specified key from Redis.
func (r *RedisStorage) Get(key string) (string, error) {
	val, err := r.client.Get(context.Background(), key).Result()
	if err == redis.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return val, nil
}

// Set sets the value for a specified key in Redis.
func (r *RedisStorage) Set(key, value string) error {
	return r.client.Set(context.Background(), key, value, 0).Err()
}

// Delete removes the key and its value from Redis.
func (r *RedisStorage) Delete(key string) error {
	_, err := r.client.Del(context.Background(), key).Result()
	return err
}
