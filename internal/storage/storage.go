package storage

// Storage defines a common interface for data storage mechanisms.
type Storage interface {
	Get(key string) (string, error)
	Set(key, value string) error
	Delete(key string) error
}
