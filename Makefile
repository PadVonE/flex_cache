PROTO_DIR=./pkg/grpcapi/proto
GO_OUT_DIR=./pkg/grpcapi
PROTO_FILES=$(wildcard $(PROTO_DIR)/*.proto)


DOCKER_IMAGE_NAME=flex_cache_app


.PHONY: init
init:
	go build -o ./bin/flex_cache_server ./cmd/server
	docker-compose up --build

.PHONY: proto
proto:
	@protoc --proto_path=$(PROTO_DIR) \
		--go_out=$(GO_OUT_DIR) \
		--go-grpc_out=$(GO_OUT_DIR) \
		--go_opt=paths=source_relative \
		--go-grpc_opt=paths=source_relative \
		$(PROTO_FILES)
	@echo "Proto files have been generated in $(GO_OUT_DIR)"


.PHONY: lint
lint:
	$(V)golangci-lint run


.PHONY: fulltest
fulltest:
	go test ./... -v

clean:
	rm -rf ./bin
	#rm -f $(GO_OUT_DIR)/*.go